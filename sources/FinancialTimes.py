from grabber import Grabber


class FinancialTimes(Grabber):
    feed_url = "https://www.ft.com/?format=rss&edition=international"
    date_format = "%a, %d %b %Y %H:%M:%S %Z"
