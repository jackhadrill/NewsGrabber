from grabber import Grabber


class Engadget(Grabber):
    feed_url = "https://www.engadget.com/rss.xml"
