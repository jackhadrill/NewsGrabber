from grabber import Grabber


class HuffingtonPostBusiness(Grabber):
    feed_url = "https://www.huffpost.com/section/business/feed"
