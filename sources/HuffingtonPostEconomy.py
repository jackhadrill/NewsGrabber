from grabber import Grabber


class HuffingtonPostEconomy(Grabber):
    feed_url = "https://www.huffpost.com/section/economy/feed"
