from grabber import Grabber


class BBCTechnology(Grabber):
    feed_url = "http://feeds.bbci.co.uk/news/technology/rss.xml"
    date_format = "%a, %d %b %Y %H:%M:%S %Z"
