from grabber import Grabber


class TheEconomistFinance(Grabber):
    feed_url = "https://www.economist.com/finance-and-economics/rss.xml"
