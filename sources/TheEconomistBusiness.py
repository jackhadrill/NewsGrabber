from grabber import Grabber


class TheEconomistBusiness(Grabber):
    feed_url = "https://www.economist.com/business/rss.xml"
