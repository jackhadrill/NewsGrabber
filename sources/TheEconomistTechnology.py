from grabber import Grabber


class TheEconomistTechnology(Grabber):
    feed_url = "https://www.economist.com/science-and-technology/rss.xml"
