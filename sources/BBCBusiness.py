from grabber import Grabber


class BBCBusiness(Grabber):
    feed_url = "http://feeds.bbci.co.uk/news/business/rss.xml"
    date_format = "%a, %d %b %Y %H:%M:%S %Z"
