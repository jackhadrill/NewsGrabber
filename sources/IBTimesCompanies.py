from grabber import Grabber


class IBTimesCompanies(Grabber):
    feed_url = "http://www.ibtimes.co.uk/rss/companies"
    date_format = "%Y-%m-%dT%H:%M:%S%z"
