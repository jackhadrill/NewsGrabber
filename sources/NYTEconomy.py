from grabber import Grabber


class NYTEconomy(Grabber):
    feed_url = "http://rss.nytimes.com/services/xml/rss/nyt/Economy.xml"
