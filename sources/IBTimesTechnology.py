from grabber import Grabber


class IBTimesTechnology(Grabber):
    feed_url = "http://www.ibtimes.co.uk/rss/technology"
    date_format = "%Y-%m-%dT%H:%M:%S%z"
