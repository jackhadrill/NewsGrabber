from grabber import Grabber


class NYTBusiness(Grabber):
    feed_url = "http://rss.nytimes.com/services/xml/rss/nyt/Business.xml"
